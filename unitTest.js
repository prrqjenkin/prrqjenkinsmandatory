var returnJSONResults = function(baseName, queryName) {
     var XMLPath = "";
     var rawJSON = loadXMLDoc(XMLPath);
    function loadXMLDoc(filePath) {
        var fs = require('fs');
        var xml2js = require('xml2js');
		var json = require('json');
		var HashMap = require('hashmap');
		var request = require('request');
		var jsonResult;
        try {
				
					var username = "Jenkins-prrq.gen";
					var password = "jenkins@prrq";
					var url = "https://ci4.cisco.com/job/IT-GATS-IT_Architecture/job/CDA/job/cda-web-app/job/bugfix/235/testReport/api/json?pretty=true";
					var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

					request(
						{
							url : url,
							headers : {
								"Authorization" : auth
							}
						},
						function (error, response, body) {
							//console.log(error);
							//console.log(response);
							var obj = JSON.parse(body);
							var testSuite = obj.childReports[0].result.suites;
							var i =0; 
							
							
							var classMap = new HashMap();
							
							for(i=0;i<testSuite.length;i++){ 
							var cases = testSuite[i].cases;
							var className = testSuite[i].name;
							var shortName = className.split(".");
							var sName = shortName[shortName.length-1];
							
							var cnt = 0;
							var skip=0;
							var pass=0;
							var fail=0;
							var values = new Array();
							for(cnt=0;cnt<cases.length;cnt++){
							var singleTest = cases[cnt];
							if(singleTest.status == "PASSED"){ pass++;}
							if(singleTest.status == "SKIPPED"){ skip++;}
							if(singleTest.status == "FAILED"){ fail++;}
							}
							values.push(pass);
							values.push(skip);
							values.push(fail);
							classMap.set(sName,values);
							}
							var heading1 = "<B>Unit Test Case report of review id : "+ process.env.reviewId+ " generated on " +new Date()+"<B>\n";
							var printVal = heading1 + "<table border=1 class='custmTable'><tr><th>Class name</th><th>Passed</th><th>Skipped</th><th>Failed</th></tr>";
							classMap.forEach(function(value, key) {
							var list = value;
							printVal = printVal+"<tr><td>"+key+"</td><td>"+list[0]+"</td><td>"+list[1]+"</td><td>"+list[2]+"</td></tr>";
							
							});	
							printVal = printVal+"</table><link rel='stylesheet' type='text/css' href='css/popupTable.css'/>"
							console.log(printVal);							
							request.post({
							url: 'http://prrq-dev.cloudapps.cisco.com/prrq/webapi/carousel',
							json: true,
							body: {
							carousel_descr:printVal,
							review_id: process.env.reviewId
							}
						});
						}
					);
			
			       return json;
    } catch (ex) {console.log(ex)}
 }
}();