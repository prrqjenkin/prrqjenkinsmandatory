var returnJSONResults = function(baseName, queryName) {
     var XMLPath = "prrq_war/target/sonar/findbugs-result.xml";
     var rawJSON = loadXMLDoc(XMLPath);
    function loadXMLDoc(filePath) {
        var fs = require('fs');
        var xml2js = require('xml2js');
		var json = require('json');
		var HashMap = require('hashmap');
		var request = require('request');
		var jsonResult;
        try {
            var fileData = fs.readFileSync(filePath, 'ascii');
            var parser = new xml2js.Parser();
			parser.parseString(fileData.substring(0, fileData.length), function (err, result) {
            var jsonarray1 = JSON.stringify(result);
			//var dependArr = result.BugCollection.BugInstance;
			var dependencyArr = result.BugCollection.BugInstance;
			var passedClassName = process.env.classNames;
			//console.log(dependencyArr[0].SourceLine[0].$.sourcefile);
			var description="<B>Static Analysis report of review id: " + process.env.reviewId +" generated on " +new Date()+"<B>\n";
			var splitClassNames = passedClassName.split(",");
			var cnt = 0;
			var map = new HashMap();
			for(cnt=0;cnt<splitClassNames.length;cnt++){
			var name = splitClassNames[cnt];
			dependencyArr.forEach(function(value, key){
			var classname = value.SourceLine[0].$.sourcefile;
			if(classname == name){
			var value = value.LongMessage;
			if(map.has(classname)){
			var arr = map.get(classname);
			arr.push('\n'+value);
			} else{
			 var arr = new Array();
			 arr.push(value);
			 map.set(classname,arr);
			} // else closing bracket
			} // indexOf closing bracket
			});
			
			
			//console.log(description);
			} // end of for loop of Class names 
			
			
			description = description + "<table border=1 class='custmTable'>";
			map.forEach(function(value, key) {
			description = description + "<tr><td> <b>Class Name: "+key+"</b></td></tr>";
			var k=0;
			for(k=0;k<value.length;k++){
			var p = value[k];
			description = description + "<tr><td>"+p+"</td></tr>";
			}
			
			});
			description = description + "</table><link rel='stylesheet' type='text/css' href='css/popupTable.css'/>";
			
			request.post({
			url: 'http://prrq-dev.cloudapps.cisco.com/prrq/webapi/carousel',
			json: true,
			body: {
			carousel_descr:description,
			review_id: process.env.reviewId 
			}
			});
			
			});
			     
        return json;
    } catch (ex) {console.log(ex)}
 }
}();